Source: r-cran-dynlm
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-r,
               r-base-dev,
               r-cran-zoo,
               r-cran-car,
               r-cran-lmtest
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-dynlm
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-dynlm.git
Homepage: https://cran.r-project.org/package=dynlm
Rules-Requires-Root: no

Package: r-cran-dynlm
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R package for dynamic linear models and time series regression
 This R package provides a user-friendly interface for fitting dynamic linear
 models and time series regression relationships
 .
 The interface and internals of dynlm are very similar to lm, but currently
 dynlm offers three advantages over the direct use of lm:
  - extended formula processing;
  - preservation of time series attributes;
  - instrumental variables regression (via two-stage least squares).
